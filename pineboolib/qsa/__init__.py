from .qsa import *  # noqa: F401
from typing import TypeVar

AnyStr = TypeVar("AnyStr", str, bytes)
