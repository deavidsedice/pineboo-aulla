# -*- coding: utf-8 -*-
from .pnfieldmetadata import PNFieldMetaData
from typing import List

# Completa Si


class PNRelationMetaDataList:
    """
    Lista de relaciones
    """

    PNRelationMetaDataList: List[PNFieldMetaData] = []
