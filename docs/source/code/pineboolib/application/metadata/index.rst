Package metadata
===============================================================

.. automodule:: pineboolib.application.metadata
    :members:
    :undoc-members:

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   pncompoundkeymetadata
   pnfieldmetadata
   pnfieldmetadatalist
   pnrelationmetadata
   pnrelationmetadatalist
   pntablemetadata
