Package plugins
===============================================================

.. automodule:: pineboolib.plugins
    :members:
    :undoc-members:

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   dgi/index
   mainform/index
   sql/index
   test/index
