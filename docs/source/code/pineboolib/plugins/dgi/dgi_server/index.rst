Package dgi_server
===============================================================

.. automodule:: pineboolib.plugins.dgi.dgi_server
    :members:
    :undoc-members:

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   dgi_server
