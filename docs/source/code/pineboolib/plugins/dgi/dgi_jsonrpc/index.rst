Package dgi_jsonrpc
===============================================================

.. automodule:: pineboolib.plugins.dgi.dgi_jsonrpc
    :members:
    :undoc-members:

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   dgi_jsonrpc
