Package dgi
===============================================================

.. automodule:: pineboolib.plugins.dgi
    :members:
    :undoc-members:

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   dgi_aqnext/index
   dgi_fcgi/index
   dgi_jsonrpc/index
   dgi_qt/index
   dgi_schema
   dgi_server/index
