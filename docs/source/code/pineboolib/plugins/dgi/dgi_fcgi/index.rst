Package dgi_fcgi
===============================================================

.. automodule:: pineboolib.plugins.dgi.dgi_fcgi
    :members:
    :undoc-members:

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   dgi_fcgi
