Module checkbox
===============================================================

.. automodule:: pineboolib.plugins.dgi.dgi_qt.dgi_objects.checkbox
    :members:
    :undoc-members:
