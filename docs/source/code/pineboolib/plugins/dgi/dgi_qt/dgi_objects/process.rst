Module process
===============================================================

.. automodule:: pineboolib.plugins.dgi.dgi_qt.dgi_objects.process
    :members:
    :undoc-members:
