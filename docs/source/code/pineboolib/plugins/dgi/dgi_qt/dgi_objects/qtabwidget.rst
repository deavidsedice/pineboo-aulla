Module qtabwidget
===============================================================

.. automodule:: pineboolib.plugins.dgi.dgi_qt.dgi_objects.qtabwidget
    :members:
    :undoc-members:
