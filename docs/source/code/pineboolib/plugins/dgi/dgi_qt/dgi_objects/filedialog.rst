Module filedialog
===============================================================

.. automodule:: pineboolib.plugins.dgi.dgi_qt.dgi_objects.filedialog
    :members:
    :undoc-members:
