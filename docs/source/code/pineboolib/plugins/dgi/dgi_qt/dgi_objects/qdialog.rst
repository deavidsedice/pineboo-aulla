Module qdialog
===============================================================

.. automodule:: pineboolib.plugins.dgi.dgi_qt.dgi_objects.qdialog
    :members:
    :undoc-members:
