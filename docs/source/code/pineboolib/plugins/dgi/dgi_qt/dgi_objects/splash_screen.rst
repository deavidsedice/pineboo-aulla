Module splash_screen
===============================================================

.. automodule:: pineboolib.plugins.dgi.dgi_qt.dgi_objects.splash_screen
    :members:
    :undoc-members:
