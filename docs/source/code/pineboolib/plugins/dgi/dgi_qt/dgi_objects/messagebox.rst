Module messagebox
===============================================================

.. automodule:: pineboolib.plugins.dgi.dgi_qt.dgi_objects.messagebox
    :members:
    :undoc-members:
