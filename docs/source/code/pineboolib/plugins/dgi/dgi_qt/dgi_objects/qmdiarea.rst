Module qmdiarea
===============================================================

.. automodule:: pineboolib.plugins.dgi.dgi_qt.dgi_objects.qmdiarea
    :members:
    :undoc-members:
