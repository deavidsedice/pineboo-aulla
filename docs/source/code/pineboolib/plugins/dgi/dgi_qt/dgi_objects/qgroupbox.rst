Module qgroupbox
===============================================================

.. automodule:: pineboolib.plugins.dgi.dgi_qt.dgi_objects.qgroupbox
    :members:
    :undoc-members:
