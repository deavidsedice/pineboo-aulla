Module about_pineboo
===============================================================

.. automodule:: pineboolib.plugins.dgi.dgi_qt.dgi_objects.dlg_about.about_pineboo
    :members:
    :undoc-members:
