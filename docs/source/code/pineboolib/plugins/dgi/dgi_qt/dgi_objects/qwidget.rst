Module qwidget
===============================================================

.. automodule:: pineboolib.plugins.dgi.dgi_qt.dgi_objects.qwidget
    :members:
    :undoc-members:
