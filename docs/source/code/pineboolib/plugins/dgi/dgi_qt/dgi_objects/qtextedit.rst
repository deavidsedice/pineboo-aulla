Module qtextedit
===============================================================

.. automodule:: pineboolib.plugins.dgi.dgi_qt.dgi_objects.qtextedit
    :members:
    :undoc-members:
