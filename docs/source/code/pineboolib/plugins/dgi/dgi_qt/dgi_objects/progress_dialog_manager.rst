Module progress_dialog_manager
===============================================================

.. automodule:: pineboolib.plugins.dgi.dgi_qt.dgi_objects.progress_dialog_manager
    :members:
    :undoc-members:
