Package dlg_about
===============================================================

.. automodule:: pineboolib.plugins.dgi.dgi_qt.dgi_objects.dlg_about
    :members:
    :undoc-members:

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   about_pineboo
