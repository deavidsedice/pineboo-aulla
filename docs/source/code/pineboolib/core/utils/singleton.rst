Module singleton
===============================================================

.. automodule:: pineboolib.core.utils.singleton
    :members:
    :undoc-members:
