Package core
===============================================================

.. automodule:: pineboolib.core
    :members:
    :undoc-members:

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   decorators
   error_manager
   exceptions
   message_manager
   parsetable
   settings
   utils/index
