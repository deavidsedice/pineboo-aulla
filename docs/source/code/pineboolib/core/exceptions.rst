Module exceptions
===============================================================

.. automodule:: pineboolib.core.exceptions
    :members:
    :undoc-members:
