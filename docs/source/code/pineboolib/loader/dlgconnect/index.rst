Package dlgconnect
===============================================================

.. automodule:: pineboolib.loader.dlgconnect
    :members:
    :undoc-members:

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   dlgconnect
