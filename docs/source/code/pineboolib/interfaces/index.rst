Package interfaces
===============================================================

.. automodule:: pineboolib.interfaces
    :members:
    :undoc-members:

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   cursoraccessmode
   dgi_schema
   iapicursor
   iconnection
   ifieldmetadata
   imanager
   isqlcursor
   itablemetadata
