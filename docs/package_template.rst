Package %MODNAME%
===============================================================

.. automodule:: %MODULE%
    :members:
    :undoc-members:

.. toctree::
   :maxdepth: 2
   :caption: Contents:
